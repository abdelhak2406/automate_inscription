from  selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re
from dotenv import load_dotenv
import os
import yagmail


# envirnment variables
load_dotenv()
EMAIL= os.getenv("EMAIL")
PASS = os.getenv("PASS")
PATH= "chromeWebDriver/93/chromedriver_linux64/chromedriver"
senders_list= os.getenv("EMAIL_RECEIVERS").split(",")



def send_email(senders_list):
    pwd = os.getenv("PASS_GMAIL")
    sender_email=os.getenv("GMAIL")
    yag = yagmail.SMTP(sender_email,password=pwd)

    sub = 'RENDEZ VOUS TCF  DISPONIBLE!! '
    bod = """Gogogogogo go il faut prendre un rdv viitteee,\n email envoye avec automatiquement \n Cordialement."""
    yag.send(to=senders_list,subject=sub,contents=bod)
    print("mail sent")

def play_something():
    os.system("mpv https://youtu.be/Cm-LyRgTYe0")

def fill_form(driver,email=EMAIL,password=PASS):
    # full the form
    search_email = driver.find_element_by_name("email")
    search_email.send_keys(email)

    search_pass= driver.find_element_by_name("password")
    search_pass.send_keys(password)
    #click enter
    search_pass.send_keys(Keys.RETURN)

def main():
    #open browser
    driver = webdriver.Chrome(PATH)
    #get the url
    driver.get(os.getenv("WEBSITE"))
    fill_form(driver)

    cpt = 0
    while(cpt != 5):
        time.sleep(5)
        try:
            # get le fil d'actualite
            list_news = WebDriverWait(driver,10).until(
                EC.presence_of_element_located((By.CLASS_NAME,"tab-content"))
            )
            print("we are inside try\n",list_news.text)

            # la on a le fil d'actualite
            #list_news = driver.find_element_by_class_name("tab-content")
            #print("--------------------------------")
            if ( re.search("minutes",list_news.text) or re.search("minute",list_news.text) or re.search("secondes",list_news.text)
                    or re.search("seconde",list_news.text)):
                send_email(senders_list)
                play_something()
                cpt = cpt+1
            # sleep for  some minutes
            time.sleep(100)
            driver.refresh()



        except:
            #play some music
            os.system("mpv https://youtu.be/dgxjIn_YBzM")
            print("NOTHING NEW !!!\n "*2,"\nYou should worry your prog doesn\'t work well!!\n")
        print("------------------------\n"*2)




if __name__ == "__main__":
    main()

