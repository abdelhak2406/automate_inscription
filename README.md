# Install dependencies

Before that we recommand to create a virtual envirnement see more [here](https://docs.python.org/3/library/venv.html)
after that activate the envirnment and
1. install the required dependecies :
    ```
    pip3 install -r requirement.txt
    ```

2. download [ChromeWebDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads/) make sure do download the correct version for you're google chrome browser. After that add the binary inside your project and put the path to it inside the `PATH` variabl in the `main.py`
 

- you also need the `mpv` media player  
    - on Ubuntu you can install it by running
    ```
        sudo apt install mpv
    ```
# run

- create a `.env` file and put your password and email adress into it
example :
    ```
    EMAIL="email_of_website_account@gmail.com"
    PASS="password_of_website"
    GMAIL="mail_adress_of_sender@mail.com"
    PASS_GMAIL="password_of_gmail"
    EMAIL_RECEIVERS="mail1@gmail.com,mail2@gmail.com"
    WEBSITE="https://lesite.com/"
    ```
    make sur to use the `,` between everty email in the email receivers variable
- run the python main script 
    ```
     python3 main.py
    ```
- **Or** run the bash script
    ```./run.sh```
    the only difference is that the bash script activate the envirnment before running `main.py`

## NOTE

you might want to delete the lines of code when i call the `mpv` media player if you don't want to use it, and also if it doesn't work because i'm just using linux and it have not been tested with windows.

## about the sending emails part:

- I followed  [this tutorial](https://realpython.com/python-send-email/)
- I used the [yagmail](https://pypi.org/project/yagmail/)  library for sending emails with gmail.
 

